//import {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

//props or properties act as a function parameter
/*
props = courseProp : { 
    description : "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore."
    id : "wdc001"
    name : "PHP - Laravel"
    onOffer : true
    price : 45000
}
*/
export default function CourseCard({courseProp}) {

	// console.log(props);
	// console.log(typeof props);
	//Object Deconstructio
	const {name, description, price, _id} = courseProp;
 		//getter, setter
		//getter = stores the value (varaible)
		//setter = it sets the value to be stored in the getter
	// const [count, setCount] = useState(0); //0 is the initial getter value
	// const [seats, setSeats] = useState(30);

	// function enroll() {
		
	// if (seats > 0) {
	// 	setCount(count + 1);
	// 	setSeats(seats - 1)
	// 	console.log('Enrollees: '+ count);
	// } else {

	// 	alert('No more seats.');
	// }
	// }

	return(				
		<Card>
	      	<Card.Body>
	       			 <Card.Title>{name}</Card.Title>
	       				 <Card.Subtitle>Description: </Card.Subtitle>
	         			  <Card.Text>{description}</Card.Text>
	         			  <Card.Subtitle>Price:</Card.Subtitle>
	         			  <Card.Text>{price}</Card.Text>
	       				 <Button variant="primary" as={Link} to={`/courseView/${_id}`}>Details</Button>
	     	</Card.Body>
	    </Card>		
	)
}

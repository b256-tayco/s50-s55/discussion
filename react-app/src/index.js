import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
//import the bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css';
const root = ReactDOM.createRoot(document.getElementById('root'));
//App component is our mother component, this is the component we use as entry point and where we can render all other components or pages.
root.render(
  <React.StrictMode>
      <App />
  </React.StrictMode>
);

/*const name = "John Smith";
const user = {
  firstName: "Jane",
  lastName: "Smith"
}

function formatName(user){
  return user.firstName + ' ' + user.lastName;
}


const element = <h1>Hello, {name} and {formatName(user)}</h1>

//render()- displays the react elements/components into the root
root.render(element);
*/


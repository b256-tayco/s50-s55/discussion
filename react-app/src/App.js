
import './App.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
/*The `BrowserRouter` component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.
The `Routes` component holds all our Route components. It selects which `Route` component to show based on the URL Endpoint.*/
import{ BrowserRouter as Router, Route, Routes } from 'react-router-dom' ;
import { UserProvider } from './userContext';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlight from './components/Highlight';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import CourseView from './pages/CourseView'


function App() {

   // State hook for the user to store the info for a global scope
   // Initialized as an object with properties from the localStorage
   // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [user,setUser] = useState({
      id: null,
      isAdmin: null
  });

  //Function for clearing the localStorage when a user logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user)
    console.log(localStorage)
  })

  return (
     //Fragments <>if 2 or more components,pages or html elements present in the code</>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
     {/*Self closing tags*/}
    <AppNavBar /> 
    <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/courseView/:courseId" element={<CourseView />} />
               {/* * means all */}
            <Route path="*" element={<NotFound />} />

          </Routes>
    </Container>
    </Router>
    </UserProvider>
  );
}

export default App;

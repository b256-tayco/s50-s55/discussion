import Highlight from '../components/Highlight';
import Banner from '../components/Banner';


export default function Home() {

    const data = {
        title: "Zuitt Coding Bootcamp",
        content: "Opportunities for everyone, everywhere",
        destination: "/courses",
        label: "Enroll now!"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlight />
        </>
    )
}

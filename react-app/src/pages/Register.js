
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Register() {

	const { user } = useContext(UserContext);

    const navigate = useNavigate();

	const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(() => {

       
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, password1, password2]);



    function registerUser(e) {

        e.preventDefault();

		fetch(`http://localhost:4000/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data === true){

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email.'   
                });

            } else {

                fetch(`http://localhost:4000/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(data === true){

                      
                        setFirstName('');
                        setLastName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to Zuitt!'
                        });

                        navigate("/courses");

                    } else {

                        Swal.fire({
                            title: 'Something wrong',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                    };

                })
            };

        })
    }

   return (
		(user.id !== null)?
		<Navigate to="/courses"/>
		:
		<>
		<h1>Register</h1>

		<Form onSubmit={e => registerUser(e)}>

		  <Form.Group className="mb-3" controlId="formBasicFirstName">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control type="firstName" placeholder="Enter first name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
	      </Form.Group>
	      
	      <Form.Group className="mb-3" controlId="formBasicLastName">
	        <Form.Label>Last Name</Form.Label>
	        <Form.Control type="lastName" placeholder="Enter last name" value={lastName} onChange={e => setLastName(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicMobileNo">
	        <Form.Label>Mobile Number</Form.Label>
	        <Form.Control type="mobileNo" placeholder="Enter Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
	     </Form.Group>

	     <Form.Group className="mb-3" controlId="formBasicPassword2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
	     </Form.Group>

	      <Button variant={isActive ? "success" : "danger"} type="submit" id="submitBtn" disabled={!isActive}>Submit</Button>
	      

	     </Form>
		 </>
	    
	)


}
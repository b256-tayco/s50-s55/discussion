import React from 'react';

//Create a context object
//A context object is a data type of an object that can be used to store info that can be shared 

const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;
